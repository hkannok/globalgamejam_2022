using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactor : MonoBehaviour
{
    [SerializeField]
    private Interactable.Character character = Interactable.Character.Mom;

    [SerializeField]
    private GameObject arrow;

    [SerializeField]
    private float range = 5;
    [SerializeField]
    private LayerMask mask;

    public List<Item> GrabbedItems { set; get; }

    private PlayerInput playerInput;
    private List<Interactable> interactablesInRange;

    void Start()
    {
        playerInput = GetComponent<PlayerInput>();
        GrabbedItems = new List<Item>();

        interactablesInRange = new List<Interactable>();
    }

    void Update()
    {
        if (playerInput.InteractPressed)
        {
            foreach (var interactable in interactablesInRange)
            {
                interactable.Interact(character);
            }
            interactablesInRange.Clear();
        }
    }

    private void FixedUpdate()
    {
        Collider2D[] hits = Physics2D.OverlapCircleAll(transform.position, range, mask.value);
        interactablesInRange.Clear();
        arrow.transform.position = Vector3.one * -1000;
        foreach (var hit in hits)
        {
            Interactable interactable = hit.GetComponent<Interactable>();
            if (interactable != null)
            {
                if (interactable.character == character)
                {
                    arrow.transform.position = interactable.transform.position + new Vector3(0, 2, 0);
                    interactablesInRange.Add(interactable);
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
