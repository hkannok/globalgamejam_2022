using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Interactable : MonoBehaviour
{
    public enum Character
    {
        Both,
        Mom,
        Kid
    }

    [SerializeField]
    public Character character;

    public UnityEvent OnInteractSuccess;
    public UnityEvent OnInteractFail;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    public void Interact(Character interactingCharacter)
    {
        bool shouldInteract = false;
        if (character != Character.Both)
        {
            if (interactingCharacter == character)
            {
                shouldInteract = true;
            }
        }
        else
        {
            shouldInteract = true;
        }

        if (shouldInteract)
        {
            OnInteractSuccess?.Invoke();
        }
        else {
            OnInteractFail?.Invoke();
        }
    }
}
