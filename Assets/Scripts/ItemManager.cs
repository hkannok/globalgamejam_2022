using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ItemManager : MonoBehaviour
{
    public static ItemManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ItemManager>();
            }
            return _instance;
        }
    }
    private static ItemManager _instance;
    [SerializeField]
    private RectTransform container;
    [SerializeField]
    private Image uiItemPrefab;

    private Item[] items;

    [SerializeField]
    private UnityEvent OnGatheredAllItems;

    [SerializeField]
    private float delayBeforeWin;


    // Start is called before the first frame update
    void Start()
    {
        items = FindObjectsOfType<Item>();
        foreach (Transform child in container)
        {
            Destroy(child.gameObject);
        }

        foreach (var item in items)
        {
            var img = Instantiate(uiItemPrefab, container);
            img.sprite = item.uiIcon;
            item.uiImage = img;
            item.OnItemGrabbed += CheckWinCondition;
        }
    }

    private void OnDestroy()
    {
        foreach (var item in items)
        {
            item.OnItemGrabbed -= CheckWinCondition;
        }
    }

    private void CheckWinCondition()
    {
        bool didWin = true;
        foreach (var item in items)
        {
            if (!item.IsGrabbed)
            {
                didWin = false;
                break;
            }
        }
        if (didWin)
        {
            StartCoroutine(CallWinCoroutine());
        }

        IEnumerator CallWinCoroutine()
        {
            yield return new WaitForSeconds(delayBeforeWin);
            OnGatheredAllItems?.Invoke();
        }
    }

    public bool HasItem(Item item)
    {
        return item.IsGrabbed;
    }

}
