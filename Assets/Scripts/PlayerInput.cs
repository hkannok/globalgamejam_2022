using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class PlayerInput : MonoBehaviour
{
    public float HorizontalAxis { get; set; }
    public bool JumpPressed { get; set; }
    public bool InteractPressed { get; set; }

    [SerializeField]
    private int playerId = 0;

    private Player player;

    void Start()
    {
        player = ReInput.players.GetPlayer(playerId);
    }

    void Update()
    {
        HorizontalAxis = player.GetAxis("Horizontal");
        InteractPressed = player.GetButtonDown("Interact");
        JumpPressed = player.GetButtonDown("Jump");
    }
}
