using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ItemCheck : MonoBehaviour
{
    [SerializeField]
    private Item itemToCheck;
    [SerializeField]
    private UnityEvent OnPassCheck;
    [SerializeField]
    private UnityEvent OnFailCheck;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Check() {
        bool result = ItemManager.Instance.HasItem(itemToCheck);
        if (result)
        {
            OnPassCheck?.Invoke();
        }
        else {
            OnFailCheck?.Invoke();
        }
    }
}
