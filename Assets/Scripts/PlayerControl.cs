using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour
{
    [SerializeField]
    private float speed = 5;
    [SerializeField]
    private float jumpForce = 10;
    [SerializeField]
    private float gravity = 10;

    [SerializeField]
    private Transform[] groundChecks;
    [SerializeField]
    private LayerMask groundCheckMask;

    private PlayerInput playerInput;
    private Rigidbody2D _rigidbody;

    private float verticalSpeed;
    private bool shouldApplyJump;

    void Start()
    {
        playerInput = GetComponent<PlayerInput>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        verticalSpeed -= Time.deltaTime * gravity;
        if (GroundCheck())
        {
            Jump();
            verticalSpeed = 0;
        }
    }

    private void FixedUpdate()
    {
        if (shouldApplyJump)
        {
            verticalSpeed = jumpForce;
            shouldApplyJump = false;
        }
        _rigidbody.velocity = new Vector2(playerInput.HorizontalAxis * speed, verticalSpeed);
    }

    private bool GroundCheck()
    {
        foreach (var check in groundChecks)
        {
            var hit = Physics2D.Linecast(transform.position, check.position, groundCheckMask.value);

            if (hit.collider != null)
            {
                Debug.DrawLine(transform.position, check.position, Color.green);
                return true;
            }
            Debug.DrawLine(transform.position, check.position, Color.red);
        }
        return false;
    }

    private void Jump()
    {
        if (playerInput.JumpPressed)
        {
            shouldApplyJump = true;
        }
    }

}
