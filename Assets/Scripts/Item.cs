using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Item : MonoBehaviour
{
    [SerializeField]
    public Sprite uiIcon;

    public Image uiImage { get; set; }

    public bool IsGrabbed { get; private set; }

    public UnityAction OnItemGrabbed;
    void Start()
    {

    }

    public void PickItem()
    {
        uiImage.transform.GetChild(0).gameObject.SetActive(true);
        IsGrabbed = true;
        OnItemGrabbed?.Invoke();
        Destroy(gameObject);
    }
}
