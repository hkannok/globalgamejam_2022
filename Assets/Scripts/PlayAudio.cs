using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAudio : MonoBehaviour
{

    // Update is called once per frame
    public void PlayOneShot(AudioClip clip)
    {
        AudioManager.Instance.PlayOneShot(clip);
    }
}
